""" bessel_3d.py: 3D Analytic Solution

Uses Bessel functions and Legendre functions to calculate the E field in a
layered dielectric sphere to solve the Hemholtz equation as shown in
Chapter 6 in Harrington

"""

import cmath
from collections import namedtuple
from math import pi, sqrt, acos, atan2, sin, cos

import matplotlib.pyplot as plt
import numpy as np

Layer = namedtuple('Layer', ('radius',
                             'sigma',
                             'epsilon'))

Sphere = namedtuple('Sphere', ('radius',
                               'k',
                               'y',
                               'sigma',
                               'epsilon_r'))

Coefficients = namedtuple('Bessel', ('bn',
                                     'cn',
                                     'dn',
                                     'en'))


def main(n_partitions,
         n_points,
         center_location,
         layers,
         freq,
         cell_size):
    """

    Args:
        n_partitions: number of partitions chosen for the simulation
        n_points: number of points at which the E field is calculated
        center_location: center location of the sphere
        layers: a list of the layers (each layer has a radius,
            sigma, epsilon)
        freq: frequency of the simulation
        cell_size: cell size in meters

    Returns:
        ez_saved: array of the e-field in the z direction for n_points

    """
    n_layers = len(layers)
    omega = 2 * pi * freq

    sphere = setup_sphere(layers, omega)

    bessel_coefficients = find_bessel_coefficients(n_partitions,
                                                   n_layers,
                                                   sphere)

    ez_saved = calculate_e(n_points, center_location, sphere, n_layers,
                           n_partitions, bessel_coefficients, cell_size,
                           omega)
    return ez_saved


def setup_sphere(layers, omega):
    """

    This function creates Sphere which has arrays of length number of
    layers for each of the parameters of the of the sphere.

    Args:
        layers: a list of the layers (each layer has a radius,
            sigma, epsilon)
        omega: 2 * pi * frequency

    Returns:
        namedtuple Sphere (arrays for each radius, epsilon_r, sigma, k,
        and y; these contain information about each layer of the sphere)

    """
    n_layers = len(layers)

    k = np.zeros(n_layers + 1, dtype='complex')
    y = np.zeros(n_layers + 1, dtype='complex')

    epsilon0 = 8.854e-12

    radius = np.zeros(n_layers + 1)
    epsilon_r = np.zeros(n_layers + 1)
    sigma = np.zeros(n_layers + 1)

    for idx, layer in enumerate(layers):
        complement_layer = n_layers - 1 - idx

        radius[complement_layer] = layer.radius
        epsilon_r[complement_layer] = layer.epsilon
        sigma[complement_layer] = layer.sigma

    k[n_layers] = (1 + 0j) * omega / 2.99793e8
    k[:-1] = np.sqrt(
        (epsilon_r[:-1] - 1j * sigma[:-1] /
         (omega * epsilon0)) /
        (1 + 0j)) * k[n_layers]

    y[n_layers] = 1j * omega * epsilon0 * (1 + 0j)
    y[:-1] = y[n_layers] * (
        (epsilon_r[:-1] - 1j * sigma[:-1] / (omega * epsilon0)) / (1 + 0j))

    return Sphere(
        radius=radius,
        epsilon_r=epsilon_r,
        sigma=sigma,
        k=k,
        y=y
    )


def find_bessel_coefficients(n_partitions, n_layers, sphere):
    """
    This calculates the Bessel coefficients from Chapter 6 in Harrington
    (see problem 6-25).  The Bessel and Hankel functions returned from
    the function 'bessel' are labeled *_k1 and *_k2 to denote whether they
    were calculated with k*r of the layer associated with the current
    radius (*_k1) or the k*r of the next outermost layer (*_k2).

    Args:
        n_partitions: number of partitions chosen for the calculation
        n_layers: number of layers in the sphere
        sphere: namedtuple Sphere (arrays for each radius, epsilon_r,
            sigma, k, and y; these contain information about each layer
            of the sphere)

    Returns:
        Coefficients: named tuple consisting of the 4 Bessel coefficients

    """
    bn = np.zeros((n_layers + 1, n_partitions), dtype='complex')
    cn = np.zeros((n_layers + 1, n_partitions), dtype='complex')
    dn = np.zeros((n_layers + 1, n_partitions), dtype='complex')
    en = np.zeros((n_layers + 1, n_partitions), dtype='complex')

    bn[0, :] = 1 + 0j
    cn[0, :] = 0 + 0j
    dn[0, :] = 1 + 0j
    en[0, :] = 0 + 0j

    for layer in range(0, n_layers):
        radius = sphere.radius[layer]
        k1 = sphere.k[layer]
        k2 = sphere.k[layer + 1]

        jn_k1, hn_k1 = bessel(k1 * radius, n_partitions)
        jn_k2, hn_k2 = bessel(k2 * radius, n_partitions)

        for partition in range(0, n_partitions):
            jn_d_k1 = -jn_k1[partition + 1] + (
                (partition + 2) / (k1 * radius)) * \
                      jn_k1[partition]

            hn_d_k1 = -hn_k1[partition + 1] + (
                (partition + 2) / (k1 * radius)) * \
                      hn_k1[partition]

            jn_d_k2 = -jn_k2[partition + 1] + (partition + 2) / (
                k2 * radius) * jn_k2[partition]

            hn_d_k2 = -hn_k2[partition + 1] + (partition + 2) / (
                k2 * radius) * hn_k2[partition]

            bn[layer + 1, partition] = calc_coefficient(layer, partition,
                                                        bn, cn, -k2 / k1,
                                                        hn_d_k2, jn_k1,
                                                        hn_k2, jn_d_k1,
                                                        hn_k1, hn_d_k1)
            cn[layer + 1, partition] = calc_coefficient(layer, partition,
                                                        bn, cn, k2 / k1,
                                                        -jn_d_k2, jn_k1,
                                                        jn_k2, jn_d_k1,
                                                        hn_k1, hn_d_k1)

            dn[layer + 1, partition] = calc_coefficient(layer, partition,
                                                        dn, en, -k1 / k2,
                                                        hn_d_k2, jn_k1,
                                                        hn_k2, jn_d_k1,
                                                        hn_k1, hn_d_k1)
            en[layer + 1, partition] = calc_coefficient(layer, partition,
                                                        dn, en, k1 / k2,
                                                        -jn_d_k2, jn_k1,
                                                        jn_k2, jn_d_k1,
                                                        hn_k1, hn_d_k1)

    for partition in range(0, n_partitions):
        an = (1j ** (-(partition + 1))) * (2.0 * (partition + 1) + 1) / (
            (partition + 1) * (partition + 2))

        cn1 = an / bn[n_layers, partition]
        cn2 = an / dn[n_layers, partition]

        bn[:, partition] = bn[:, partition] * cn1
        cn[:, partition] = cn[:, partition] * cn1
        dn[:, partition] = dn[:, partition] * cn2
        en[:, partition] = en[:, partition] * cn2

    return Coefficients(bn, cn, dn, en)


def calc_coefficient(layer, partition, c1, c2, k_ratio,
                     func1, func2, func3, func4, func5, func6):
    """ Calculate a coefficient at a given partition / layer """
    return (
        c1[layer, partition] * 1j * (func1 * func2[partition] +
                                     k_ratio * func3[partition] * func4) +
        c2[layer, partition] * 1j * (func1 * func5[partition] +
                                     k_ratio * func3[partition] * func6))


def bessel(kr, n_partitions):
    """
    This calculates some of the Bessel functions and Hankel functions based
    off Chapter 6 / Appendix D in Harrington.

    Args:
        kr: k * r at the current layer
        n_partitions: number of partitions chosen

    Returns:
        jn: Bessel functions of the first kind
        hn: Hankel functions of the first kind

    """
    m = n_partitions + 3
    jn = np.zeros(m + 1, dtype='complex')
    jn[m - 1] = 1 + 0j

    for partition in range(m - 1, -1, -1):
        jn[partition - 1] = ((2.0 * (partition + 1) + 1) * jn[
            partition] / kr) - jn[partition + 1]

    k = ((cmath.sin(kr) / (kr ** 2)) - cmath.cos(kr) / kr) / jn[0]

    jn[:n_partitions + 1] = jn[:n_partitions + 1] * k
    jn = kr * jn

    hn = np.zeros(n_partitions + 1, dtype='complex')
    hn[0] = -cmath.cos(kr) / (kr ** 2) - cmath.sin(kr) / kr
    hn[1] = ((1 / kr) - 3.0 / (kr ** 3)) * cmath.cos(kr) \
            - 3.0 / (kr ** 2) * cmath.sin(kr)

    for partition in range(1, n_partitions):
        hn[partition + 1] = ((2 * (partition + 1) + 1)
                             * hn[partition] / kr) - hn[partition - 1]

    hn = jn[:n_partitions + 1] - 1j * kr * hn

    return jn[:n_partitions + 1], hn


def calculate_e(n_points, center_location, sphere, n_layers,
                n_partitions, bessel_coeff, cell_size, omega):
    """
    Iterates through each location to calculate the electric field in the z
    direction

    Args:
        n_points: number of points at which the E field is calculated
        center_location: center location of the sphere
        sphere: namedtuple Sphere (arrays for each radius, epsilon_r,
            sigma, k, and y; these contain information about each layer
            of the sphere)
        n_layers: number of layers in the sphere
        n_partitions: number of partitions chosen for the simulation
        bessel_coeff: named tuple consisting of the 4 Bessel coefficients
        cell_size: cell size in meters

    Returns:
        An array of ez at every location

    """
    ez_saved = np.zeros(n_points)

    for point in range(n_points - 1, -1, -1):
        current_r, theta, phi, cond, layer = \
            find_location_parameters(point, center_location, cell_size,
                                     n_layers, sphere)

        sum_r, sum_theta_1, sum_theta_2, sum_phi_1, sum_phi_2 = \
            calculate_sums(n_partitions, layer, current_r, sphere,
                           bessel_coeff, omega, n_layers, theta)

        e_r = cos(phi) * sin(theta) / (sphere.y[layer] * current_r ** 2) \
              * sum_r
        e_theta = cos(phi) / current_r * (sum_theta_2 - sum_theta_1)
        e_phi = sin(phi) / current_r * (sum_phi_1 - sum_phi_2)

        ez_saved[point] = abs(convert_to_cartesian(e_r, e_theta, e_phi,
                                                   theta, phi))

    return ez_saved


def find_location_parameters(point, center_location, cell_size, n_layers,
                             sphere):
    """

    Args:
        point: the cell at which the calculation is occurring
        center_location: center location of the sphere
        cell_size: cell size in meters
        n_layers: number of layers in the sphere
        sphere: namedtuple Sphere (arrays for each radius, epsilon_r,
            sigma, k, and y; these contain information about each layer
            of the sphere)

    Returns:
        current_r: distance of the current location to the center
            of the sphere
        theta: angle theta at current point
        phi: angle phi at current point
        cond: conductivity of the current layer
        layer: the layer at the given radius

    """
    xc = 0.0
    zc = 0.005
    yc = ((point + 1) - center_location) * cell_size
    current_r = sqrt(xc ** 2 + yc ** 2 + zc ** 2)
    theta = pi - acos(yc / current_r)
    phi = atan2(xc, zc)

    cond = find_current_cond(n_layers, current_r, sphere)
    layer = find_current_layer(n_layers, current_r, sphere)

    return current_r, theta, phi, cond, layer


def find_current_cond(n_layers, rad, sphere):
    """ Returns the conductivity at the input radius

    Args:
        n_layers: number of layers in the sphere
        rad: distance of the current location to the center of the sphere
        sphere: namedtuple Sphere (arrays for each radius, epsilon_r,
            sigma, k, and y; these contain information about each layer
            of the sphere)

    Returns:
        Conductivity of the layer at the current location

    """
    cond = sphere.sigma[0]

    for layer in range(0, n_layers):
        if rad >= sphere.radius[layer]:
            cond = sphere.sigma[layer + 1]

    return cond


def find_current_layer(n_layers, rad, sphere):
    """ Returns the layer at the input radius

    Args:
        n_layers: number of layers in the sphere
        rad: distance of the current location to the center of the sphere
        sphere: namedtuple Sphere (arrays for each radius, epsilon_r,
            sigma, k, and y; these contain information about each layer
            of the sphere)

    Returns:
        the layer at the given radius

    """
    for layer, this_radius in enumerate(sphere.radius[:n_layers]):
        if rad <= this_radius:
            return layer
    return n_layers


def calculate_sums(n_partitions, layer, current_r, sphere,
                   bessel_coeff, omega, n_layers, theta):
    """

    This function calculated 5 sums used to calculate the electric field in
    the E_r, E_theta, and E_phi directions (see Eq. 6-26, Problem 6-25 in
    Harrington) using the Bessel, Hankel, and Legendre functions.

    Args:
        n_partitions: number of partitions chosen for the simulation
        layer: the layer at the given radius
        current_r: distance of the current location to the center
            of the sphere
        sphere: namedtuple Sphere (arrays for each radius, epsilon_r,
            sigma, k, and y; these contain information about each layer
            of the sphere)
        bessel_coeff: named tuple consisting of the 4 Bessel coefficients
        omega: 2 * pi * frequency
        n_layers: number of layers in the sphere
        theta: angle theta at current point

    Returns:
        sum_r, sum_theta_1, sum_theta_2, sum_phi_1, sum_phi_2, each of
        which is used in the electric field calculations

    """
    jn, hn = bessel(sphere.k[layer] * current_r, n_partitions)
    pn, pn_d = legendre(theta, n_partitions)

    sum_r = sum_theta_1 = sum_theta_2 = sum_phi_1 = sum_phi_2 = 0 + 0j

    mu0 = 4 * pi * 1.0e-7

    for partition in range(0, n_partitions):
        jn_d = -jn[partition + 1] + (partition + 2) / (
            sphere.k[layer] * current_r) * jn[partition]
        hn_d = -hn[partition + 1] + (partition + 2) / (
            sphere.k[layer] * current_r) * hn[partition]

        sum_r += ((partition + 1) * (partition + 2)) / (omega * mu0) * (
            bessel_coeff.bn[layer, partition] * jn[partition] +
            bessel_coeff.cn[layer, partition] * hn[partition]) \
                 * pn[partition]

        sum_theta_1 += 1 / sphere.k[n_layers] * (
            bessel_coeff.dn[layer, partition] * jn[partition] +
            bessel_coeff.en[layer, partition] * hn[partition]) \
                       * pn[partition]

        sum_theta_2 += sphere.k[layer] / \
                       (sphere.y[layer] * omega * mu0) * (
                           bessel_coeff.bn[layer, partition] * jn_d +
                           bessel_coeff.cn[layer, partition] * hn_d) \
                       * pn_d[partition]

        sum_phi_1 += 1 / sphere.k[n_layers] * (
            bessel_coeff.dn[layer, partition] * jn[partition] +
            bessel_coeff.en[layer, partition] * hn[partition]) \
                     * pn_d[partition]

        sum_phi_2 += sphere.k[layer] / \
                     (sphere.y[layer] * omega * mu0) * (
                         bessel_coeff.bn[layer, partition] * jn_d +
                         bessel_coeff.cn[layer, partition] * hn_d) \
                     * pn[partition]

    return sum_r, sum_theta_1, sum_theta_2, sum_phi_1, sum_phi_2


def legendre(theta, n_partitions):
    """

    This calculates some of the lower-order Legendre polynomials based off
    Chapter 6 / Appendix E in Harrington.

    Args:
        theta: angle theta at current point
        n_partitions: number of partitions

    Returns:
        pn: Legendre polynomials
        pn_d: derivative of Legendre polynomials

    """
    pn = np.zeros(n_partitions)
    pn[0] = 1.0
    pn[1] = 3.0 * cos(theta)

    for partition in range(2, n_partitions):
        pn[partition] = \
            ((2 * partition + 1) * cos(theta) * pn[partition - 1]
             - (partition + 1) * pn[partition - 2]) / partition

    pn2 = np.zeros(n_partitions)
    pn2[0] = 0
    pn2[1] = 3.0 * (sin(theta) ** 2)
    pn2[2] = 15.0 * cos(theta) * (sin(theta) ** 2)

    for partition in range(3, n_partitions):
        pn2[partition] = \
            ((2 * partition + 1) * cos(theta) * pn2[partition - 1] -
             (partition + 2) * pn2[partition - 2]) / (partition - 1)
    pn_d = -pn2 + cos(theta) * pn

    return pn, pn_d


def convert_to_cartesian(e_r, e_theta, e_phi, theta, phi):
    return e_r * sin(theta) * cos(phi) + e_theta * cos(theta) \
           * cos(phi) - e_phi * sin(phi)


if __name__ == '__main__':
    ez_saved = main(
        n_partitions=8,
        n_points=20,
        center_location=11,
        layers=[Layer(radius=.1, sigma=.3, epsilon=30)],
        freq=500e6,
        cell_size=0.01
    )

    plt.plot(ez_saved[::-1])
    plt.show()

This code is used in "Electromagnetic Simulation Using the FDTD Method with Python, Third Edition" 
by Jennifer E. Houle and Dennis M. Sullivan.

Published by John Wiley & Sons, Inc., Hoboken, New Jersey, 2020.